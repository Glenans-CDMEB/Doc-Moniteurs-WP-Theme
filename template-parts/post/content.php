<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    if (is_sticky() && is_home()) :
        echo twentyseventeen_get_svg(array('icon' => 'thumb-tack'));
    endif;
    ?>
    <header class="entry-header">
		<?php
			if ( 'post' === get_post_type() ) :
				echo '<div class="entry-meta">';
					if ( is_single() ) :
						twentyseventeen_posted_on();
					else :
						echo twentyseventeen_time_link();
						twentyseventeen_edit_link();
					endif;
				echo '</div><!-- .entry-meta -->';
			endif;

			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} elseif ( is_front_page() && is_home() ) {
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		?>
	</header><!-- .entry-header -->

    <?php if ('' !== get_the_post_thumbnail() && !is_single()) : ?>
        <div class="post-thumbnail">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('twentyseventeen-featured-image'); ?>
            </a>
        </div><!-- .post-thumbnail -->
    <?php endif; ?>

    <div class="entry-content">
        <?php
        /* translators: %s: Name of current post */
        the_content(sprintf(
            __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen'),
            get_the_title()
        ));

        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'twentyseventeen'),
            'after' => '</div>',
            'link_before' => '<span class="page-number">',
            'link_after' => '</span>',
        ));
        ?>
    </div><!-- .entry-content -->

    <?php if (get_field('author_name') && get_field('author_email') && get_field('document_date')) : ?>
    <div class="cdmeb_cidoc_meta">
        <?php
        // 7 is the id of tag "Glénans_OFFICIEL"
        if (has_tag('7')) :
        ?>
            <p><span class="glenans-officiel">Document Officiel</span></p>
        <?php endif; ?>
        <dl>
            <dt>Auteur&nbsp;: </dt>
            <dd><?php the_field('author_name') ?></dd>
            <dt>Contact de l'auteur&nbsp;: </dt>
            <dd><?php the_field('author_email') ?></dd>
            <dt>Date du document&nbsp;: </dt>
            <dd>
                <?php
                    // Affichage de type: "3 février 2017"
                    setlocale(LC_ALL, 'fr_FR.UTF-8');
                    $tzParis = new DateTimeZone('Europe/Paris');
                    $myTimeStamp = new DateTime(get_field('document_date'), $tzParis);
                    echo strftime("%e %B %Y", $myTimeStamp->getTimestamp());
                ?>
            </dd>
        </dl>
    </div><!-- .cdmeb_cidoc_meta -->
    <?php endif; ?>
    
    <?php if (is_single()) : ?>
        <?php twentyseventeen_entry_footer(); ?>
    <?php endif; ?>

</article><!-- #post-## -->
