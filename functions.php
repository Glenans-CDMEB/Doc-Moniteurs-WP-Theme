<?php

/**
 * INIT: Register Child Theme (CSS)
 */

function my_theme_enqueue_styles()
{
    $parent_style = 'twentyseventeen-style';

    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array($parent_style),
        wp_get_theme()->get('Version')
    );
}

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');

/**
 * Flush rewrite-rules to prevent 404
 * @See http://www.wpexplorer.com/post-type-404-error/
 */
function my_flush_rewrite_rules()
{
    flush_rewrite_rules();
}

add_action('after_switch_theme', 'my_flush_rewrite_rules');

/**
 * Custom Post Type : Fiche lecture
 */

if (!function_exists('cdmeb_ci_doc')) {
// Register Custom Post Type
    function cdmeb_ci_doc()
    {
        $labels = array(
            'name' => _x('Fiche lecture', 'Post Type General Name', 'cdmeb'),
            'singular_name' => _x('Fiche lecture', 'Post Type Singular Name', 'cdmeb'),
            'menu_name' => __('Fiches lecture', 'cdmeb'),
            'name_admin_bar' => __('Fiche Lecture', 'cdmeb'),
            'archives' => __('Fiche Lecture Archives', 'cdmeb'),
            'attributes' => __('Fiche Lecture Attributes', 'cdmeb'),
            'parent_item_colon' => __('', 'cdmeb'),
            'all_items' => __('All Fiches lecture', 'cdmeb'),
            'add_new_item' => __('Add Fiche Lecture', 'cdmeb'),
            'add_new' => __('Add New Fiche Lecture', 'cdmeb'),
            'new_item' => __('New Fiche Lecture', 'cdmeb'),
            'edit_item' => __('Edit Fiche Lecture', 'cdmeb'),
            'update_item' => __('Update Fiche Lecture', 'cdmeb'),
            'view_item' => __('View Fiche Lecture', 'cdmeb'),
            'view_items' => __('View Fiches lecture', 'cdmeb'),
            'search_items' => __('Search Fiche Lecture', 'cdmeb'),
            'not_found' => __('Not found', 'cdmeb'),
            'not_found_in_trash' => __('Not found in Trash', 'cdmeb'),
            'featured_image' => __('Featured Image', 'cdmeb'),
            'set_featured_image' => __('Set featured image', 'cdmeb'),
            'remove_featured_image' => __('Remove featured image', 'cdmeb'),
            'use_featured_image' => __('Use as featured image', 'cdmeb'),
            'insert_into_item' => __('Insert into Fiche Lecture', 'cdmeb'),
            'uploaded_to_this_item' => __('Uploaded to this Fiche Lecture', 'cdmeb'),
            'items_list' => __('Fiches lecture list', 'cdmeb'),
            'items_list_navigation' => __('Fiches lecture list navigation', 'cdmeb'),
            'filter_items_list' => __('Filter Fiches lecture list', 'cdmeb'),
        );
        $args = array(
            'label' => __('Fiche lecture', 'cdmeb'),
            'description' => __('Identity Card of Document', 'cdmeb'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', 'post-formats',),
            'taxonomies' => array('category', 'post_tag'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 3,
            'menu_icon' => 'dashicons-book',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        );
        register_post_type('cidoc', $args);
    }

    add_action('init', 'cdmeb_ci_doc', 0);
}

/**
 * Add Fiche Lecture to "visible" documents depending on template
 * @see https://wpchannel.com/custom-post-types-pages-auteurs-wordpress/
 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts#Include_Custom_Post_Types_in_Search_Results
 * @see https://developer.wordpress.org/files/2014/10/template-hierarchy.png
 * @see https://codex.wordpress.org/Conditional_Tags
 */
function add_my_post_types_to_query($query)
{
    if (!is_admin() && $query->is_main_query()) {
        if (is_home() || is_archive() || is_single() || is_author())
            $query->set('post_type', array('post', 'page', 'cidoc'));
    }
    return $query;
}

add_action('pre_get_posts', 'add_my_post_types_to_query');

/**
 * The following function is overriden from the parent theme to include new post type "cidoc"
 * 2017-05-07 mfaure
 */
if (!function_exists('twentyseventeen_entry_footer')) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function twentyseventeen_entry_footer()
    {

        /* translators: used between list items, there is a space after the comma */
        $separate_meta = __(', ', 'twentyseventeen');

        // Get Categories for posts.
        $categories_list = get_the_category_list($separate_meta);

        // Get Tags for posts.
        $tags_list = get_the_tag_list('', $separate_meta);

        // We don't want to output .entry-footer if it will be empty, so make sure its not.
        if (((twentyseventeen_categorized_blog() && $categories_list) || $tags_list) || get_edit_post_link()) {

            echo '<footer class="entry-footer">';

            // HERE is the diff with parent theme (just the post-type test)
            if ('post' === get_post_type() || 'cidoc' === get_post_type()) {
                if (($categories_list && twentyseventeen_categorized_blog()) || $tags_list) {
                    echo '<span class="cat-tags-links">';

                    // Make sure there's more than one category before displaying.
                    if ($categories_list && twentyseventeen_categorized_blog()) {
                        echo '<span class="cat-links">' . twentyseventeen_get_svg(array('icon' => 'folder-open')) . '<span class="screen-reader-text">' . __('Categories', 'twentyseventeen') . '</span>' . $categories_list . '</span>';
                    }

                    if ($tags_list) {
                        echo '<span class="tags-links">' . twentyseventeen_get_svg(array('icon' => 'hashtag')) . '<span class="screen-reader-text">' . __('Tags', 'twentyseventeen') . '</span>' . $tags_list . '</span>';
                    }

                    echo '</span>';
                }
            }

            twentyseventeen_edit_link();

            echo '</footer> <!-- .entry-footer -->';
        }
    }
endif;
